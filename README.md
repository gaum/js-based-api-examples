# JS Based API Examples



## Contents

Here are several standalone HTML files that use some Javascript or JS Framework, as well as some API calling framework. Each item has a "Loading..." div, as well as an error message div. Results are logged in console.


## Current Implementations

jQuery + Ajax

Vue 3 + Axios

Javascript + Fetch

React + Axios
